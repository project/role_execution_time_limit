-- SUMMARY --
Role execution time limit is a small module that
allow you to set PHP max execution time per role
and command line usage(drush).

-- REQUIREMENTS --
Permission to change max_execution_time on your webserver.


-- INSTALLATION --

  * Drupal 7
  Install as usual.
  See http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

  * Drupal 7
  Go to admin/people/permissions#module-role_execution_time_limit
  to set the persmission.
  
  Go to admin/config/development/role_execution_time_limit
  to set the limit for a role.
